package com.einsurance.insurence.requestBody;

import lombok.Data;

@Data
public class PasswordRequest {
	private String oldPassword; 
	  
	 private String newPassword;
}
