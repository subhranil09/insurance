package com.einsurance.insurence.requestBody;

import lombok.Data;

@Data
public class Credentials {

	private String email;
	private String password;
}
