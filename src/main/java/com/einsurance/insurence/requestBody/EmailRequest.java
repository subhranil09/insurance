package com.einsurance.insurence.requestBody;

import lombok.Data;

@Data
public class EmailRequest {
	private String email;
	private String subject;
	private String body;
}
