package com.einsurance.insurence.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.einsurance.insurence.model.CommissionAccount;
import com.einsurance.insurence.model.Customer;
import com.einsurance.insurence.model.InsuranceAccount;

public interface InsuranceAccountRepository extends JpaRepository<InsuranceAccount, Long> {
	List<InsuranceAccount> findAllByCustomer(Customer customer);
}
