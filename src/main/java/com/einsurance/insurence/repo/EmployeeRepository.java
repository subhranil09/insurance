package com.einsurance.insurence.repo;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.einsurance.insurence.model.Employee;

public interface EmployeeRepository extends JpaRepository<Employee, Long> {
	Optional<Employee> findAllByEmailIdAndPassword(String emailId, String password);
}
