package com.einsurance.insurence.repo;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.einsurance.insurence.model.Agent;
import com.einsurance.insurence.model.CommissionAccount;

public interface CommissionAccountRepository extends JpaRepository<CommissionAccount, Long>{
	CommissionAccount findByAgentId(long agentId);
	Optional<CommissionAccount> findByAgentCode(String agentCode);
}
