package com.einsurance.insurence.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.einsurance.insurence.model.Payment;

public interface PaymentRepository extends JpaRepository<Payment, Long>{
	List<Payment> findAllByCustomerId(long customerId);
}
