package com.einsurance.insurence.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.einsurance.insurence.model.CommissionWithdrawal;

public interface CommissionWithdrawalRepository extends JpaRepository<CommissionWithdrawal, Long>{
	List<CommissionWithdrawal> findAllByAgentId(long agentId);
}
