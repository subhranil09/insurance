package com.einsurance.insurence.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.einsurance.insurence.model.Commission;

public interface CommissionRepository extends JpaRepository<Commission, Long>{
	List<Commission> findAllByAgentId(long agentId);
}
