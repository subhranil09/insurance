package com.einsurance.insurence.exceptions;

import lombok.Getter;

public class CommissionNotPresentException extends Exception {

	private static final long serialVersionUID = -7149808455321634570L;
	@Getter 
	 private String errorMsg; 
	 
	 public CommissionNotPresentException() { 
	  super(); 
	  this.errorMsg = "Commission Not Present!!"; 
	 }
}
