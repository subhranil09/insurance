package com.einsurance.insurence.exceptions;

import lombok.Getter;

public class CommissionAccountNotPresentException extends Exception {

	
	private static final long serialVersionUID = -2462509833021060289L;
	@Getter 
	 private String errorMsg; 
	 
	 public CommissionAccountNotPresentException() { 
	  super(); 
	  this.errorMsg = "Commission Account Not Present!!"; 
	 }
}
