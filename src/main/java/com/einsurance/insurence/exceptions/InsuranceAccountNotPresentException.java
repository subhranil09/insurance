package com.einsurance.insurence.exceptions;

import lombok.Getter;

public class InsuranceAccountNotPresentException extends Exception {

	private static final long serialVersionUID = 5458107224329513426L;
	  
	 @Getter 
	 private String errorMsg; 
	 
	 public InsuranceAccountNotPresentException() { 
	  super(); 
	  this.errorMsg = "InsuranceAccount Not Present!!"; 
	 } 

}
