package com.einsurance.insurence.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.einsurance.insurence.exceptions.CommissionAccountNotPresentException;
import com.einsurance.insurence.model.CommissionAccount;
import com.einsurance.insurence.repo.CommissionAccountRepository;

@Service
public class CommissionAccountServiceImpl implements CommissionAccountService {
	@Autowired
	CommissionAccountRepository commissionAccountRepository;
	@Override
	public CommissionAccount addCommissionAccount(CommissionAccount commissionAccount) {
		CommissionAccount newsavedCommissionAccount = commissionAccountRepository.save(commissionAccount);
		return newsavedCommissionAccount;
	}
	@Override
	public CommissionAccount getCommissionAccountById(long commissionAccountId)
			throws CommissionAccountNotPresentException {
		Optional<CommissionAccount> commissionAccountOp = commissionAccountRepository.findById(commissionAccountId);
		if (!commissionAccountOp.isPresent()) {
			throw new CommissionAccountNotPresentException();
		}
		return commissionAccountOp.get();
	}

	@Override
	public List<CommissionAccount> getCommissionAccounts() {
		List<CommissionAccount> commissionAccounts = commissionAccountRepository.findAll();
		return commissionAccounts;
	}

}
