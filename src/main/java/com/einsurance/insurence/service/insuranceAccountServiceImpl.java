package com.einsurance.insurence.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.einsurance.insurence.exceptions.InsuranceAccountNotPresentException;
import com.einsurance.insurence.exceptions.UserNotValidException;
import com.einsurance.insurence.model.CommissionAccount;
import com.einsurance.insurence.model.Customer;
import com.einsurance.insurence.model.InsuranceAccount;
import com.einsurance.insurence.model.InsuranceSettings;
import com.einsurance.insurence.model.Payment;
import com.einsurance.insurence.repo.CustomerRepository;
import com.einsurance.insurence.repo.InsuranceAccountRepository;

@Service
public class insuranceAccountServiceImpl implements InsuranceAccountService {
	@Autowired
	CustomerService customerService;
	@Autowired
	CustomerRepository customerRepository;
	@Autowired
	InsuranceAccountRepository insuranceAccountRepository;
	@Autowired
	InsuranceSettingsService InsuranceSettingsService;
	@Autowired
	PaymentService paymentService;
	@Override
	public InsuranceAccount addInsuranceAccount(InsuranceAccount insuranceAccount, long customerId) throws UserNotValidException, InsuranceAccountNotPresentException {
		Customer customer = customerService.getCustomerById(customerId);
		InsuranceSettings setting = InsuranceSettingsService.getSettingById(1);
		insuranceAccount.setStatus("active");
		customer.addInsuranceAccount(insuranceAccount);
		Customer savedCustomer = customerRepository.save(customer);
		List<InsuranceAccount> insuranceAccounts = savedCustomer.getInsuranceAccounts();
		InsuranceAccount insuranceAccountNew = insuranceAccounts.get(insuranceAccounts.size()-1);
		long insuranceAccountId = insuranceAccountNew.getInsuranceAccountId();
		double installmentAmount=insuranceAccountNew.getTotalPremiumAmount()/insuranceAccountNew.getPolicyTerm();
		for(int i=1;i<=insuranceAccountNew.getPolicyTerm();i++) {
			if(i==1) {
				Payment payment=new Payment();
				payment.setInstallmentAmount(installmentAmount);
				payment.setCustomerId(insuranceAccount.getCustomer().getCustomerId());
				payment.setInstallmentDate(insuranceAccountNew.getDateCreated()+(1000*60*60*24*30*i));
				payment.setPaidDate(insuranceAccountNew.getDateCreated()+(1000*60*60*24*30*i));
				payment.setInstallmentNo(i);
				payment.setPaymentStatus("paid");
				payment.setTaxAmount(installmentAmount*(setting.getTaxPercentage()/100));
				paymentService.addPayment(payment, insuranceAccountId);
			}
			else {
			Payment payment=new Payment();
			payment.setInstallmentAmount(installmentAmount);
			payment.setCustomerId(insuranceAccount.getCustomer().getCustomerId());
			payment.setInstallmentDate(insuranceAccountNew.getDateCreated()+(1000*60*60*24*30*i));
			payment.setInstallmentNo(i);
			payment.setPaymentStatus("due");
			payment.setTaxAmount(installmentAmount*(setting.getTaxPercentage()/100));
			paymentService.addPayment(payment, insuranceAccountId);
			}
		}
		
		return insuranceAccountNew;
	}

	@Override
	public InsuranceAccount getInsuranceAccountById(long insuranceAccountId)
			throws InsuranceAccountNotPresentException {
		Optional<InsuranceAccount> insuranceAccount = insuranceAccountRepository.findById(insuranceAccountId);
		if(!insuranceAccount.isPresent())
		{
			throw new InsuranceAccountNotPresentException();
		}
		return insuranceAccount.get();
	}

	

	@Override
	public List<InsuranceAccount> getInsuranceAccountByCustomerId(long customerId) {
		Optional<Customer> customer = customerRepository.findById(customerId);
		List<InsuranceAccount> findAllByCustomerId = insuranceAccountRepository.findAllByCustomer(customer.get());
		return findAllByCustomerId;
	}

	@Override
	public List<InsuranceAccount> getInsuranceAccounts() {
		List<InsuranceAccount> accounts = insuranceAccountRepository.findAll();
		return accounts;
	}

}
