package com.einsurance.insurence.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.einsurance.insurence.exceptions.UserNotValidException;
import com.einsurance.insurence.model.Customer;
import com.einsurance.insurence.repo.CustomerRepository;
import com.einsurance.insurence.requestBody.Credentials;
import com.einsurance.insurence.requestBody.PasswordRequest;
import com.einsurance.insurence.responseBody.LoginResponse;


@Service
public class CustomerServiceImpl implements CustomerService {
	@Autowired
	CustomerRepository customerRepository;
	@Override
	public Customer addCustomer(Customer customer) {
		customer.setRole("ROLE_Customer");
		Customer savedCustome = customerRepository.save(customer);
		return savedCustome;
	}
	@Override
	public List<Customer> getCustomer() {
		List<Customer> customerList = customerRepository.findAll();
		return customerList;
	}
	@Override
	public LoginResponse login(Credentials credentials) throws UserNotValidException {
		Optional<Customer> customersOptional=customerRepository.findAllByEmailIdAndPassword(credentials.getEmail(),credentials.getPassword());
		if(!customersOptional.isPresent())
		{
			throw new UserNotValidException("Username & Password invalid"); 
		}
		Customer customer = customersOptional.get();
		LoginResponse loginResponse=new LoginResponse();
		loginResponse.setId(customer.getCustomerId());
		loginResponse.setName(customer.getCustomerName());
		loginResponse.setRole(customer.getRole());
		return loginResponse;
		
	}
	@Override
	public Customer getCustomerById(long customerId) throws UserNotValidException {
		Optional<Customer> customer = customerRepository.findById(customerId);
		if(!customer.isPresent())
		{
			throw new UserNotValidException("CustomerNotPresent"); 
		}
		return customer.get();
	}
	
	@Override 
	 public Customer changePassword(PasswordRequest passwordRequest,long userId) throws UserNotValidException { 
	  Optional<Customer> customer = customerRepository.findById(userId); 
	 
	  if (!customer.isPresent()) { 
	   throw new UserNotValidException("customeer not valid"); 
	  } 
	  Customer emp1 = customer.get(); 
	  if (emp1.getPassword().equals(passwordRequest.getOldPassword())) { 
	   emp1.setPassword(passwordRequest.getNewPassword()); 
	  } 
	  Customer savedCustomer = customerRepository.save(emp1); 
	 
	  return savedCustomer; 
	 }

}
