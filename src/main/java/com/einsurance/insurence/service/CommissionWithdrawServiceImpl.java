package com.einsurance.insurence.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.einsurance.insurence.exceptions.CommissionAccountNotPresentException;
import com.einsurance.insurence.model.CommissionAccount;
import com.einsurance.insurence.model.CommissionWithdrawal;
import com.einsurance.insurence.model.InsuranceType;
import com.einsurance.insurence.repo.CommissionAccountRepository;
import com.einsurance.insurence.repo.CommissionWithdrawalRepository;

@Service
public class CommissionWithdrawServiceImpl implements CommissionWithdrawService{
	@Autowired
	CommissionWithdrawalRepository commissionWithdrawalRepository;
	@Autowired 
	CommissionAccountService commissionAccountService;
	@Autowired
	CommissionAccountRepository commissionAccountRepository;

	@Override
	public CommissionWithdrawal addCommissionWithdrawal(CommissionWithdrawal commissionWithdrawal,
			long commissionAccountId) throws CommissionAccountNotPresentException {
		CommissionAccount commissionAccount = commissionAccountService.getCommissionAccountById(commissionAccountId);
		commissionWithdrawal.setAgentId(commissionAccount.getAgentId());
		commissionAccount.addCommissionWithdrawal(commissionWithdrawal);
		commissionAccount.setBalance(0);
		commissionAccountRepository.save(commissionAccount);
		return commissionWithdrawal;
	}

	@Override
	public List<CommissionWithdrawal> getCommissionWithdrawals() {
		return commissionWithdrawalRepository.findAll();
	}

	@Override
	public List<CommissionWithdrawal> getCommissionWithdrawalsByAgentId(long agentId) {
		return commissionWithdrawalRepository.findAllByAgentId(agentId);
	}
	

}
