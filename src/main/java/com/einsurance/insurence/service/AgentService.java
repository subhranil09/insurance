package com.einsurance.insurence.service;

import java.util.List;

import com.einsurance.insurence.exceptions.UserNotValidException;
import com.einsurance.insurence.model.Agent;
import com.einsurance.insurence.requestBody.Credentials;
import com.einsurance.insurence.requestBody.PasswordRequest;
import com.einsurance.insurence.responseBody.LoginResponse;

public interface AgentService {
	Agent addAgent(Agent agent);
	Agent updateAgent(Agent agent);

	List<Agent> getAgent();
	
	Agent getuserById(long agentId) throws UserNotValidException;

	LoginResponse login(Credentials credentials) throws UserNotValidException;

	Agent changePassword(PasswordRequest passwordRequest, long userId) throws UserNotValidException;
}
