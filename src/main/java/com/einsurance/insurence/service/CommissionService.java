package com.einsurance.insurence.service;

import java.util.List;

import com.einsurance.insurence.exceptions.CommissionNotPresentException;
import com.einsurance.insurence.exceptions.SchemeNotPresentException;
import com.einsurance.insurence.exceptions.UserNotValidException;
import com.einsurance.insurence.model.Commission;
import com.einsurance.insurence.model.CommissionAccount;

public interface CommissionService{
	Commission addCommission(Commission commission ,String agentCode) throws UserNotValidException; 
	 
	Commission installmentCommission(Commission commission ,String agentCode,long insuranceSchemeId) throws SchemeNotPresentException, UserNotValidException;
	CommissionAccount getCommissionById(long commissionId) throws CommissionNotPresentException; 
  
	List<Commission> getCommissions();
	List<Commission> getCommissionsByAgentId(long agentId);
}
