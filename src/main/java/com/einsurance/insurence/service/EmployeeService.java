package com.einsurance.insurence.service;

import java.util.List;

import com.einsurance.insurence.exceptions.EmployeeNotFoundException;
import com.einsurance.insurence.exceptions.UserNotValidException;
import com.einsurance.insurence.model.Employee;
import com.einsurance.insurence.requestBody.Credentials;
import com.einsurance.insurence.requestBody.PasswordRequest;
import com.einsurance.insurence.responseBody.LoginResponse;

public interface EmployeeService {
	Employee addEmployee(Employee employee);

	List<Employee> getEmployees();

	void deleteEmployee(long employeeId) throws EmployeeNotFoundException;

	Employee getEmployeeById(long employeeId) throws EmployeeNotFoundException;

	LoginResponse login(Credentials credential) throws UserNotValidException;

	Employee changePassword(PasswordRequest passwordRequest,long userId) throws EmployeeNotFoundException;

}
