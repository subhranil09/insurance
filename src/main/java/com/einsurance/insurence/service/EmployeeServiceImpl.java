package com.einsurance.insurence.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.einsurance.insurence.exceptions.EmployeeNotFoundException;
import com.einsurance.insurence.exceptions.UserNotValidException;
import com.einsurance.insurence.model.Employee;
import com.einsurance.insurence.repo.EmployeeRepository;
import com.einsurance.insurence.requestBody.Credentials;
import com.einsurance.insurence.requestBody.PasswordRequest;
import com.einsurance.insurence.responseBody.LoginResponse;

@Service
public class EmployeeServiceImpl implements EmployeeService {

	@Autowired
	EmployeeRepository employeeRepository;
	
	@Override
	public Employee addEmployee(Employee employee) {
		Employee savedEmployee = employeeRepository.save(employee);
		return savedEmployee;
	}
	
	

	@Override
	public List<Employee> getEmployees() {
		List<Employee> employeeList = employeeRepository.findAll();
		return employeeList;
	}

	@Override
	public void deleteEmployee(long employeeId) throws EmployeeNotFoundException {
		employeeRepository.delete(getEmployeeById(employeeId));
	}

	@Override
	public Employee getEmployeeById(long employeeId) throws EmployeeNotFoundException {
		Optional<Employee> employee = employeeRepository.findById(employeeId);
		if(!employee.isPresent()) {
			throw new EmployeeNotFoundException();
		}
		return employee.get();
	}



	@Override
	public LoginResponse login(Credentials credential) throws UserNotValidException {
		Optional<Employee> empOptional = employeeRepository.findAllByEmailIdAndPassword(credential.getEmail(),credential.getPassword());
		if(!empOptional.isPresent())
		{
			throw new UserNotValidException("Username & Password invalid"); 
		}
		 Employee employee = empOptional.get();
		LoginResponse loginResponse=new LoginResponse();
		loginResponse.setId(employee.getEmployeeId());
		loginResponse.setName(employee.getEmpoyeeName());
		loginResponse.setRole(employee.getRole());
		return loginResponse;
	}
	
	@Override 
	 public Employee changePassword(PasswordRequest passwordRequest,long userId) throws EmployeeNotFoundException { 
	  Optional<Employee> employee = employeeRepository.findById(userId); 
	   
	  if(!employee.isPresent()) { 
	   throw new EmployeeNotFoundException(); 
	  } 
	  Employee emp1 = employee.get(); 
	  if(emp1.getPassword().equals(passwordRequest.getOldPassword())) { 
	   emp1.setPassword(passwordRequest.getNewPassword()); 
	  } 
	  Employee savedEmployee = employeeRepository.save(emp1); 
	   
	  return savedEmployee; 
	 }
	

}
