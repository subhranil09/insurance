package com.einsurance.insurence.service;

import java.util.List;

import com.einsurance.insurence.exceptions.CommissionAccountNotPresentException;
import com.einsurance.insurence.model.CommissionWithdrawal;


public interface CommissionWithdrawService {
	
	CommissionWithdrawal addCommissionWithdrawal(CommissionWithdrawal commissionWithdrawal,long commissionAccountId) throws CommissionAccountNotPresentException;
	List<CommissionWithdrawal> getCommissionWithdrawals();
	
	List<CommissionWithdrawal> getCommissionWithdrawalsByAgentId(long agentId);
}
