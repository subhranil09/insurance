package com.einsurance.insurence.service;

import java.util.List;

import com.einsurance.insurence.exceptions.InsuranceAccountNotPresentException;
import com.einsurance.insurence.model.Payment;

public interface PaymentService {
	
	Payment addPayment(Payment payment, long insuranceAccountId) throws InsuranceAccountNotPresentException; 
	  
	List<Payment> getPaymentsByCustomerId(long customerIdId); 
  
	List<Payment> getPaymentByInsuranceAccountId(long insuranceAccountId);
}

