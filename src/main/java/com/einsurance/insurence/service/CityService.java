package com.einsurance.insurence.service;

import java.util.List;

import com.einsurance.insurence.exceptions.CityAlreadyExistException;
import com.einsurance.insurence.exceptions.CityNotPresentException;
import com.einsurance.insurence.exceptions.StateNotPresentException;
import com.einsurance.insurence.model.City;

public interface CityService {
	

	City updateStatus(String status, long cityId) throws CityNotPresentException;

	City getCityById(long cityId) throws CityNotPresentException;

	List<City> getCities();

	void deleteCity(long cityId) throws CityNotPresentException;

	City addCity(City city, long stateId) throws CityAlreadyExistException, StateNotPresentException;
}
