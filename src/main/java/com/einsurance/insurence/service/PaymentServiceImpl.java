package com.einsurance.insurence.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.einsurance.insurence.exceptions.InsuranceAccountNotPresentException;
import com.einsurance.insurence.model.InsuranceAccount;
import com.einsurance.insurence.model.Payment;
import com.einsurance.insurence.repo.InsuranceAccountRepository;
import com.einsurance.insurence.repo.PaymentRepository;

@Service
public class PaymentServiceImpl implements PaymentService {
	@Autowired
	InsuranceAccountService InsuranceAccountService;

	@Autowired
	InsuranceAccountRepository insuranceAccountRepository;

	@Autowired
	PaymentRepository paymentRepository;

	@Override
	public Payment addPayment(Payment payment, long insuranceAccountId) throws InsuranceAccountNotPresentException {
		InsuranceAccount insuranceAccount = InsuranceAccountService.getInsuranceAccountById(insuranceAccountId);
		insuranceAccount.addPayment(payment);
		if (payment.getPaymentStatus().equalsIgnoreCase("paid")) {
			insuranceAccount.setBalance(insuranceAccount.getBalance() + payment.getInstallmentAmount());
		}
		insuranceAccountRepository.save(insuranceAccount);
		return payment;
	}

	@Override
	public List<Payment> getPaymentsByCustomerId(long customerId) {
		List<Payment> paymentList = paymentRepository.findAllByCustomerId(customerId);
		return paymentList;
	}

	@Override
	public List<Payment> getPaymentByInsuranceAccountId(long insuranceAccountId) {

		return null;
	}

}
