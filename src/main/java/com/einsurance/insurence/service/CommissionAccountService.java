package com.einsurance.insurence.service;

import java.util.List;

import com.einsurance.insurence.exceptions.CommissionAccountNotPresentException;
import com.einsurance.insurence.model.CommissionAccount;

public interface CommissionAccountService {
		CommissionAccount addCommissionAccount(CommissionAccount commissionAccount); 
	  
		CommissionAccount getCommissionAccountById(long commissionAccountId) throws CommissionAccountNotPresentException; 
	  
		List<CommissionAccount> getCommissionAccounts();
}
