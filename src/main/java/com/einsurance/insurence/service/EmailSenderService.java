package com.einsurance.insurence.service;

import java.util.Properties;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;
import jakarta.mail.*;
import jakarta.mail.internet.InternetAddress;
import jakarta.mail.internet.MimeMessage;

@Service
public class EmailSenderService {

	@Autowired
	private MailSender emailSender;
	
	public void sendEmail(String toEmail, String body, String subject) {
		try {
		SimpleMailMessage message = new SimpleMailMessage();
		
		message.setFrom("ghosh.subhranil.it26");
		message.setTo(toEmail);
		message.setText(body);
		message.setSubject(subject);
		System.out.println("mail prev send....");

		emailSender.send(message);
		System.out.println("mail send....");
		}
		catch (Exception e) {
			System.out.println(e);
		}
	}
//	public boolean sendEmail(String to, String subject, String text) {
//        boolean flag = false;
//
//        //logic
//        //smtp properties
//        Properties properties = new Properties();
//        properties.put("mail.smtp.auth", true);
//        properties.put("mail.smtp.starttls.enable", true);
//        properties.put("mail.smtp.port", "587");
//        properties.put("mail.smtp.host", "smtp.gmail.com");
//
//        String username = "ghoshsubhranil26@gmail.com";
//        String password = "wdguppspoohhcphu";
//
//
//        //session
//        Session session = Session.getInstance(properties, new Authenticator() {
//            @Override
//            protected PasswordAuthentication getPasswordAuthentication() {
//                return new PasswordAuthentication(username, password);
//            }
//        });
//
//        try {
//
//            Message message = new MimeMessage(session);
//            message.setRecipient(Message.RecipientType.TO, new InternetAddress(to));
//            message.setFrom(new InternetAddress("ghoshsubhranil26@gmail.com"));
//            message.setSubject(subject);
//            message.setText(text);
//            Transport.send(message);
//            flag = true;
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//
//        return flag;
//    }
}
