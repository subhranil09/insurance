package com.einsurance.insurence.service;

import java.util.List;

import com.einsurance.insurence.exceptions.UserNotValidException;
import com.einsurance.insurence.model.Customer;
import com.einsurance.insurence.requestBody.Credentials;
import com.einsurance.insurence.requestBody.PasswordRequest;
import com.einsurance.insurence.responseBody.LoginResponse;



public interface CustomerService {
	Customer addCustomer(Customer customer);

	List<Customer> getCustomer();
	
	Customer getCustomerById(long customerId) throws UserNotValidException;
	LoginResponse login(Credentials credential) throws UserNotValidException;
	Customer changePassword(PasswordRequest passwordRequest,long userId) throws UserNotValidException;

}
