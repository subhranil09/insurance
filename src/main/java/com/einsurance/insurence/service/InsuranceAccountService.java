package com.einsurance.insurence.service;

import java.util.List;

import com.einsurance.insurence.exceptions.InsuranceAccountNotPresentException;
import com.einsurance.insurence.exceptions.UserNotValidException;
import com.einsurance.insurence.model.InsuranceAccount;

public interface InsuranceAccountService {
	
	InsuranceAccount addInsuranceAccount(InsuranceAccount insuranceAccount ,long customerId) throws UserNotValidException, InsuranceAccountNotPresentException; 
	  
	 InsuranceAccount getInsuranceAccountById(long insuranceAccountId) throws InsuranceAccountNotPresentException; 
	 
	 List<InsuranceAccount> getInsuranceAccountByCustomerId(long customerId);
	  
	 List<InsuranceAccount> getInsuranceAccounts();
}
