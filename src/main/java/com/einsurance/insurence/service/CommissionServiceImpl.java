package com.einsurance.insurence.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.einsurance.insurence.exceptions.CommissionNotPresentException;
import com.einsurance.insurence.exceptions.SchemeNotPresentException;
import com.einsurance.insurence.exceptions.UserNotValidException;
import com.einsurance.insurence.model.Commission;
import com.einsurance.insurence.model.CommissionAccount;
import com.einsurance.insurence.model.InsuranceScheme;
import com.einsurance.insurence.repo.CommissionAccountRepository;
import com.einsurance.insurence.repo.CommissionRepository;
@Service
public class CommissionServiceImpl implements CommissionService {

	@Autowired
	CommissionAccountService commissionAccountService;
	
	@Autowired
	CommissionAccountRepository commissionAccountRepository;
	
	@Autowired
	InsuranceSchemeService insuranceSchemeService;
	
	@Autowired
	CommissionRepository commissionRepository;
	@Override
	public Commission addCommission(Commission commission,String agentCode) throws UserNotValidException {
		Optional<CommissionAccount> commissionAccountop = commissionAccountRepository.findByAgentCode(agentCode);
		if(!commissionAccountop.isPresent()) {
			throw new UserNotValidException("Agent Not Found");
		}
		CommissionAccount commissionAccount = commissionAccountop.get();
		commission.setCommissionAmount(commission.getCommissionAmount()*(commission.getCommissionPercentage()/100));
		commissionAccount.addCommission(commission);
		commission.setAgentId(commissionAccount.getAgentId());
		commissionAccount.setBalance(commissionAccount.getBalance()+commission.getCommissionAmount());
		commissionAccountRepository.save(commissionAccount);
		return commission;
	}
	
	

	@Override
	public CommissionAccount getCommissionById(long commissionId) throws CommissionNotPresentException {
		return null;
	}

	@Override
	public List<Commission> getCommissions() {
		List<Commission> commissions = commissionRepository.findAll();
		return commissions;
	}

	@Override
	public List<Commission> getCommissionsByAgentId(long agentId) {
		List<Commission> commissions = commissionRepository.findAllByAgentId(agentId);
		return commissions;
	}



	@Override
	public Commission installmentCommission(Commission commission, String agentCode, long insuranceSchemeId) throws SchemeNotPresentException, UserNotValidException {
		InsuranceScheme insuranceScheme = insuranceSchemeService.getInsuranceSchemeById(insuranceSchemeId);
		commission.setCommissionPercentage(insuranceScheme.getCommissionForInstallmentPayment());
		Commission addCommission = addCommission(commission, agentCode);
		return addCommission;
	}
	
}
