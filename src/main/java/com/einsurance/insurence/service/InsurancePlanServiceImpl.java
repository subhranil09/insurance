package com.einsurance.insurence.service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.einsurance.insurence.exceptions.InsurancePlanalredyExistException;
import com.einsurance.insurence.exceptions.PlanNotPresentException;
import com.einsurance.insurence.exceptions.SchemeNotPresentException;
import com.einsurance.insurence.model.InsurancePlan;
import com.einsurance.insurence.model.InsuranceScheme;
import com.einsurance.insurence.repo.InsurancePlanRepository;
import com.einsurance.insurence.repo.InsuranceSchemeRepository;

@Service
public class InsurancePlanServiceImpl implements InsurancePlanService {

	@Autowired
	InsurancePlanRepository insurancePlanRepository;
	
	@Autowired
	InsuranceSchemeRepository insuranceSchemeRepository;
	@Autowired
	InsuranceSchemeService insuranceSchemeService;
	@Autowired
	InsuranceTypeService insuranceTypeService;
	
	@Override
	public InsurancePlan addInsurancePlan(InsurancePlan insurancePlan, long insuranceSchemeId ) throws InsurancePlanalredyExistException, SchemeNotPresentException {
		List<InsuranceScheme> insuranceSchemes = insuranceSchemeService.getAllInsuranceScheme();
		Optional<InsuranceScheme> insuranceSchemeop = insuranceSchemes.stream().filter(e -> e.getInsuranceSchemeId() == insuranceSchemeId).findFirst();
		if(!insuranceSchemeop.isPresent()) {
			throw new SchemeNotPresentException();
		}
		List<InsurancePlan> listOfInsurancePlans = getAllInsurancePlan();
		Optional<InsurancePlan> plans = listOfInsurancePlans.stream()
				.filter(e -> e.getInsurancePlan().equals(insurancePlan.getInsurancePlan()) && e.getInsuranceScheme().getInsuranceSchemeId() == insuranceSchemeId)
				.findFirst();
		if (plans.isPresent()) {
			throw new InsurancePlanalredyExistException();
		}
		InsuranceScheme insuranceScheme = insuranceSchemeService.getInsuranceSchemeById(insuranceSchemeId);
		insuranceScheme.addPlan(insurancePlan);
		insuranceSchemeRepository.save(insuranceScheme);
		return insurancePlan;
	}

	@Override
	public List<InsurancePlan> getAllInsurancePlan() {
		List<InsurancePlan> listOfPlans = insurancePlanRepository.findAll();
		return listOfPlans;
	}
	@Override
	public List<InsurancePlan> getActiveInsurancePlan() {
		List<InsurancePlan> listOfPlans = insurancePlanRepository.findAll();
		List<InsurancePlan> activeInsurancePlan = listOfPlans.stream()
				.filter(e -> e.getStatus().equalsIgnoreCase("active")).collect(Collectors.toList());
		return activeInsurancePlan;
	}

	@Override
	public InsurancePlan getInsurancePlanById(long insurancePlanId) throws PlanNotPresentException {
		Optional<InsurancePlan> plan = insurancePlanRepository.findById(insurancePlanId);
		if (!plan.isPresent()) {
			throw new PlanNotPresentException();
		}
			return plan.get();
	}
	@Override
	public InsurancePlan updateInsurancePlan(InsurancePlan insurancePlan) throws PlanNotPresentException {
		InsurancePlan plan = getInsurancePlanById(insurancePlan.getInsurancePlanId());
		if(plan==null)
			throw new PlanNotPresentException();
		InsurancePlan saveInsurancePlan = insurancePlanRepository.save(insurancePlan);
		return saveInsurancePlan;
	}


}
