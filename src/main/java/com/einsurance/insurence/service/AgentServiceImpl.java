package com.einsurance.insurence.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.einsurance.insurence.exceptions.UserNotValidException;
import com.einsurance.insurence.model.Agent;
import com.einsurance.insurence.model.CommissionAccount;
import com.einsurance.insurence.repo.AgentRepository;
import com.einsurance.insurence.requestBody.Credentials;
import com.einsurance.insurence.requestBody.PasswordRequest;
import com.einsurance.insurence.responseBody.LoginResponse;

@Service
public class AgentServiceImpl implements AgentService {
	@Autowired
	AgentRepository agentRepository;
	@Autowired
	CommissionAccountService commissionAccountService;

	@Override
	public Agent addAgent(Agent agent) {
		agent.setRole("ROLE_Agent");
		Agent savedAgent = agentRepository.save(agent);
		CommissionAccount commissionAccount = new CommissionAccount();
		commissionAccount.setCommissionAccountId(0);
		commissionAccount.setAgentCode(savedAgent.getAgentCode());
		commissionAccount.setAgentId(savedAgent.getAgentId());
		commissionAccount.setBalance(0);
		commissionAccountService.addCommissionAccount(commissionAccount);
		return savedAgent;
	}

	@Override
	public List<Agent> getAgent() {
		List<Agent> agentList = agentRepository.findAll();
		return agentList;
	}

	@Override

	public LoginResponse login(Credentials credentials) throws UserNotValidException {
		Optional<Agent> agentsOptional = agentRepository.findAllByEmailIdAndPassword(credentials.getEmail(),
				credentials.getPassword());
		if (!agentsOptional.isPresent()) {
			throw new UserNotValidException("Username & Password invalid");
		}
		Agent agent = agentsOptional.get();
		LoginResponse loginResponse = new LoginResponse();
		loginResponse.setId(agent.getAgentId());
		loginResponse.setName(agent.getAgentName());
		loginResponse.setRole(agent.getRole());
		return loginResponse;

	}

	@Override
	public Agent changePassword(PasswordRequest passwordRequest, long userId) throws UserNotValidException {
		Optional<Agent> agent = agentRepository.findById(userId);

		if (!agent.isPresent()) {
			throw new UserNotValidException("Agent not valid");
		}
		Agent agent2 = agent.get();
		if (agent2.getPassword().equals(passwordRequest.getOldPassword())) {
			agent2.setPassword(passwordRequest.getNewPassword());
		}
		Agent savedagent = agentRepository.save(agent2);

		return savedagent;

	}

	@Override
	public Agent getuserById(long agentId) throws UserNotValidException {
		Optional<Agent> agent = agentRepository.findById(agentId);
		if (!agent.isPresent()) {
			throw new UserNotValidException("AgentNot Present");
		}
		return agent.get();
	}

	@Override
	public Agent updateAgent(Agent agent) {
		Agent savedAgent = agentRepository.save(agent);
		return savedAgent;

	}
}
