package com.einsurance.insurence;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;

import com.einsurance.insurence.service.EmailSenderService;

@SpringBootApplication
public class InsuranceApplication {


	public static void main(String[] args) {
		SpringApplication.run(InsuranceApplication.class, args);

	}


}
