package com.einsurance.insurence.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
public class InsuranceScheme {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long insuranceSchemeId;
	@NotBlank
	private String insuranceScheme;
//	private String insuranceType;
	private double commissionForNewRegistration;
	private double commissionForInstallmentPayment;
	private String note;
	@NotBlank
	private String status;
	
	
	@ManyToOne(cascade = {CascadeType.DETACH,CascadeType.MERGE,CascadeType.PERSIST,CascadeType.REFRESH})
	@JoinColumn(name="insurance_type_id")
	private InsuranceType insuranceType;
	
	@JsonIgnore
	@OneToMany(cascade = {CascadeType.DETACH,CascadeType.MERGE,CascadeType.PERSIST,CascadeType.REFRESH},mappedBy = "insuranceScheme")
	private List<InsurancePlan> insurancePlans;
	
	public void addPlan(InsurancePlan insurancePlan) {
		if(insurancePlans==null) {
			insurancePlans=new ArrayList<InsurancePlan>();
		}
		insurancePlans.add(insurancePlan);
		insurancePlan.setInsuranceScheme(this);
	}
}
