package com.einsurance.insurence.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Entity
@NoArgsConstructor
public class CommissionAccount {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long commissionAccountId;

	private String agentCode;
	@Column(unique = true)
	private long agentId;
	
	private double balance;

	@JsonIgnore
	@OneToMany(cascade = {CascadeType.DETACH,CascadeType.MERGE,CascadeType.PERSIST,CascadeType.REFRESH},mappedBy = "commissionAccount")
	private List<CommissionWithdrawal> commissionWithdrawals;
	
	public void addCommissionWithdrawal(CommissionWithdrawal commissionWithdrawal) {
		if(commissionWithdrawals==null) {
			commissionWithdrawals=new ArrayList<CommissionWithdrawal>();
		}
		commissionWithdrawals.add(commissionWithdrawal);
		commissionWithdrawal.setCommissionAccount(this);;
	}
	
	@JsonIgnore
	@OneToMany(cascade = {CascadeType.DETACH,CascadeType.MERGE,CascadeType.PERSIST,CascadeType.REFRESH},mappedBy = "commissionAccount")
	private List<Commission> commissions;
	
	public void addCommission(Commission commission) {
		if(commissions==null) {
			commissions=new ArrayList<Commission>();
		}
		commissions.add(commission);
		commission.setCommissionAccount(this);;
	}
}
