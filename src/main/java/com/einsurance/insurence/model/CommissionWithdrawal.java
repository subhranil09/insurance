package com.einsurance.insurence.model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Entity
@NoArgsConstructor
public class CommissionWithdrawal {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long commissionWithdrawalId;

	private double withdrawnAmount;

	private long withdrawlDate;
	
	private long agentId;

	
	@ManyToOne(cascade = {CascadeType.DETACH,CascadeType.MERGE,CascadeType.PERSIST,CascadeType.REFRESH})
	@JoinColumn(name="commission_account_id")
	private CommissionAccount commissionAccount;
	
}
