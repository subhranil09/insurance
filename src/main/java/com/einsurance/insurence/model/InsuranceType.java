package com.einsurance.insurence.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
public class InsuranceType {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long insuranceTypeId;
	@NotBlank
	@Column(unique = true)
	private String insuranceType; 
	private String imageSrc;
	@NotBlank
	private String status;
	
	@JsonIgnore
	@OneToMany(cascade = {CascadeType.DETACH,CascadeType.MERGE,CascadeType.PERSIST,CascadeType.REFRESH},mappedBy = "insuranceType")
	private List<InsuranceScheme> insuranceSchemes;
	
	public void addScheme(InsuranceScheme insuranceScheme) {
		if(insuranceSchemes==null) {
			insuranceSchemes=new ArrayList<InsuranceScheme>();
		}
		insuranceSchemes.add(insuranceScheme);
		insuranceScheme.setInsuranceType(this);
	}
}
	