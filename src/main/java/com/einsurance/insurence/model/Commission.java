package com.einsurance.insurence.model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Entity
@NoArgsConstructor
public class Commission {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long commissionId;

	private long insuranceAccountNumber;

	private long agentId;
	
	private double commissionAmount;
	
	private double commissionPercentage;

	private long commissionDate;
	
	@ManyToOne(cascade = {CascadeType.DETACH,CascadeType.MERGE,CascadeType.PERSIST,CascadeType.REFRESH})
	@JoinColumn(name="commission_account_id")
	private CommissionAccount commissionAccount;

	
}
