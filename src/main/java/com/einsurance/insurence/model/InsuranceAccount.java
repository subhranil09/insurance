package com.einsurance.insurence.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

@Data
@Entity
public class InsuranceAccount {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long insuranceAccountId;
	private long dateCreated;
	private long maturityDate;
	private int policyTerm;
	private double totalPremiumAmount;
	private double sumAssured; 
	private long insurancePlanId;
	private long insuranceSchemeId;
	private String insurancePlan;
	private String insuranceScheme;
	@ManyToOne(cascade = {CascadeType.DETACH,CascadeType.MERGE,CascadeType.PERSIST,CascadeType.REFRESH})
	@JoinColumn(name="customer_id")
	private Customer customer;
	private String agentCode;
	private String status;
	private double balance;
	
	
	@JsonIgnore
	@OneToMany(cascade = {CascadeType.DETACH,CascadeType.MERGE,CascadeType.PERSIST,CascadeType.REFRESH},mappedBy = "insuranceAccount")
	private List<Payment> payments;
	
	public void addPayment(Payment payment) {
		if(payments==null) {
			payments=new ArrayList<Payment>();
		}
		payments.add(payment);
		payment.setInsuranceAccount(this);
	}
	
}
