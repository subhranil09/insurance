package com.einsurance.insurence.model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotBlank;

import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
public class InsurancePlan {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long insurancePlanId;
	
	@NotBlank
	private String insurancePlan;

	private int minumumPolicyTerm;
	
	private int maximumPolicyTerm;
	
	private int minimumAge;
	
	private int maximumAge;
	
	private double minimumInvestmentAmount;
	
	private double maximumInvestmentAmount;
	
	private double profitRatio;
	
	private String status; 
	
	
	@ManyToOne(cascade = {CascadeType.DETACH,CascadeType.MERGE,CascadeType.PERSIST,CascadeType.REFRESH})
	@JoinColumn(name="insurance_scheme_id")
	private InsuranceScheme insuranceScheme;

}
