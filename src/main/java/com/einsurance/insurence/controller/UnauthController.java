package com.einsurance.insurence.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.einsurance.insurence.exceptions.UserNotValidException;
import com.einsurance.insurence.model.Customer;
import com.einsurance.insurence.requestBody.Credentials;
import com.einsurance.insurence.responseBody.LoginResponse;
import com.einsurance.insurence.service.AgentService;
import com.einsurance.insurence.service.CustomerService;
import com.einsurance.insurence.service.EmployeeService;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/unauth")
public class UnauthController {
	@Autowired
    CustomerService customerService;
	@Autowired
	AgentService agentService;
	@Autowired
	EmployeeService employeeService;
	@PostMapping("/register")
	public ResponseEntity<?> addCustomer(@RequestBody Customer customer) {
		customer.setCustomerId(0);
		return new ResponseEntity<Customer>(customerService.addCustomer(customer), HttpStatus.CREATED);
		
	}
	@GetMapping("/customers")
	public ResponseEntity<?> getCustomers(){
		return new ResponseEntity<List<Customer>>(customerService.getCustomer(), HttpStatus.OK);
	}
	
	@PostMapping("/loginCustomer")
	public ResponseEntity<?> loginCustomer(@RequestBody Credentials credential) {
		
		try {
			return new ResponseEntity<LoginResponse>(customerService.login(credential), HttpStatus.CREATED);
		} catch (UserNotValidException e) {

			return new ResponseEntity<String>(e.getErrorMsg(), HttpStatus.BAD_REQUEST);
		}		
		
		
	}
	@PostMapping("/loginAgent")
	public ResponseEntity<?> loginAgent(@RequestBody Credentials credential) {
		
		try {
			System.out.println(credential);
			return new ResponseEntity<LoginResponse>(agentService.login(credential), HttpStatus.CREATED);
		} catch (UserNotValidException e) {

			return new ResponseEntity<String>(e.getErrorMsg(), HttpStatus.BAD_REQUEST);
		}		
		
		
	}
	@PostMapping("/loginEmployee")
	public ResponseEntity<?> loginEmployee(@RequestBody Credentials credential) {
		
		try {
			return new ResponseEntity<LoginResponse>(employeeService.login(credential), HttpStatus.CREATED);
		} catch (UserNotValidException e) {

			return new ResponseEntity<String>(e.getErrorMsg(), HttpStatus.BAD_REQUEST);
		}		
		
		
	}
}
