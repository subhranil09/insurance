package com.einsurance.insurence.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.einsurance.insurence.exceptions.UserNotValidException;
import com.einsurance.insurence.model.Agent;
import com.einsurance.insurence.model.Commission;
import com.einsurance.insurence.model.CommissionWithdrawal;
import com.einsurance.insurence.model.Customer;
import com.einsurance.insurence.model.InsuranceAccount;
import com.einsurance.insurence.repo.CommissionWithdrawalRepository;
import com.einsurance.insurence.requestBody.EmailRequest;
import com.einsurance.insurence.requestBody.PasswordRequest;
import com.einsurance.insurence.service.AgentService;
import com.einsurance.insurence.service.CommissionService;
import com.einsurance.insurence.service.CommissionWithdrawService;
import com.einsurance.insurence.service.CustomerService;
import com.einsurance.insurence.service.EmailSenderService;
import com.einsurance.insurence.service.FeedbackService;
import com.einsurance.insurence.service.InsuranceAccountService;
import com.einsurance.insurence.service.InsurancePlanService;
import com.einsurance.insurence.service.InsuranceTypeService;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/agent")
public class AgentController {
	@Autowired
	CustomerService customerService;
	@Autowired
	InsurancePlanService insurancePlanService;
	@Autowired
	InsuranceTypeService insuranceTypeService;
	@Autowired
	FeedbackService feedbackService;
	@Autowired
	InsuranceAccountService insuranceAccountService;
	@Autowired
	CommissionService CommissionService;
	@Autowired 
	EmailSenderService emailService;
	@Autowired
	CommissionWithdrawService commissionWithdrawService;
	@Autowired 
	AgentService agentService;
	
	@GetMapping("/getCustomers")
	public ResponseEntity<?> getCustomers() {
		return new ResponseEntity<List<Customer>>(customerService.getCustomer(), HttpStatus.OK);
	}
	@GetMapping("/getInsuranceAccounts")
	public ResponseEntity<?> getInsuranceAccountsByCustomerId( ) {
		
		return new ResponseEntity<List<InsuranceAccount>>(insuranceAccountService.getInsuranceAccounts(), HttpStatus.OK);

	}
	
	
	@PostMapping("/sendEmail") 
	 public void sendEmail(@RequestBody EmailRequest email) { 
	   emailService.sendEmail(email.getEmail(), email.getBody(), email.getSubject()); 
	 }
	@GetMapping("/getCommissionsByAgentId/{agentId}")
	public ResponseEntity<?> getCommissionsByAgentId(@PathVariable long agentId ) {
		
		return new ResponseEntity<List<Commission>>(CommissionService.getCommissionsByAgentId(agentId), HttpStatus.OK);

	}
	@PostMapping("/changePassword/{userId}") 
	 public Agent changePassword(@RequestBody PasswordRequest passwordRequest,@PathVariable String userId) throws NumberFormatException, UserNotValidException 
	   { 
	  return (agentService.changePassword(passwordRequest,Long.parseLong(userId))); 
	 }
	
	@GetMapping("/getCommissionWithdrawalsByAgentId/{agentId}")
	public ResponseEntity<?> getCommissionWithdrawalsByAgentId(@PathVariable String agentId ) {
		
		return new ResponseEntity<List<CommissionWithdrawal>>(commissionWithdrawService.getCommissionWithdrawalsByAgentId(Long.parseLong(agentId)), HttpStatus.OK);

	}
	
	@PostMapping("/addCommissionWithdrawal/{commissionAccountId}")
	public ResponseEntity<?> addCommissionWithdrawal(@RequestBody CommissionWithdrawal commissionWithdrawal,@PathVariable long commissionAccountId) {

		try {
			return new ResponseEntity<CommissionWithdrawal>(
					commissionWithdrawService.addCommissionWithdrawal(commissionWithdrawal,commissionAccountId), HttpStatus.CREATED);
		} catch (Exception e) {
			return new ResponseEntity<Exception>(e, HttpStatus.BAD_REQUEST);
		}

	}
	@GetMapping("/getAgentByByAgentId/{agentId}")
	public ResponseEntity<?> getAgentByByAgentId(@PathVariable String agentId ) {
		
		try {
			return new ResponseEntity<Agent>(agentService.getuserById(Long.parseLong(agentId)), HttpStatus.OK);
		} catch (NumberFormatException | UserNotValidException e) {
			return new ResponseEntity<Exception>(e, HttpStatus.BAD_REQUEST);
		}

	}
	@PutMapping("/update")
	public ResponseEntity<?> updateAgent(@RequestBody Agent agent) {
		return new ResponseEntity<Agent>(agentService.updateAgent(agent), HttpStatus.CREATED);
		
	}
	
}
