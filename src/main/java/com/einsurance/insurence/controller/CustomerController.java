package com.einsurance.insurence.controller;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.hibernate.engine.jdbc.StreamUtils;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.einsurance.insurence.exceptions.PlanNotPresentException;
import com.einsurance.insurence.exceptions.UserNotValidException;
import com.einsurance.insurence.model.Commission;
import com.einsurance.insurence.model.Customer;
import com.einsurance.insurence.model.Documents;
import com.einsurance.insurence.model.Feedback;
import com.einsurance.insurence.model.InsuranceAccount;
import com.einsurance.insurence.model.InsurancePlan;
import com.einsurance.insurence.model.Payment;
import com.einsurance.insurence.requestBody.PasswordRequest;
import com.einsurance.insurence.responseBody.OrderResponse;
import com.einsurance.insurence.service.CommissionService;
import com.einsurance.insurence.service.CustomerService;
import com.einsurance.insurence.service.DocumentService;
import com.einsurance.insurence.service.FeedbackService;
import com.einsurance.insurence.service.InsuranceAccountService;
import com.einsurance.insurence.service.InsurancePlanService;
import com.einsurance.insurence.service.InsuranceTypeService;
import com.einsurance.insurence.service.PaymentService;
import com.razorpay.Order;
import com.razorpay.RazorpayClient;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/customers")
public class CustomerController {
	@Value("${project.image}")
	private String path;

	@Autowired
	DocumentService documentService;

	@Autowired
	InsurancePlanService insurancePlanService;
	@Autowired
	InsuranceTypeService insuranceTypeService;
	@Autowired
	FeedbackService feedbackService;
	@Autowired
	InsuranceAccountService insuranceAccountService;

	@Autowired
	CommissionService commissionService;

	@Autowired
	CustomerService customerService;

	@Autowired
	PaymentService paymentService;

	@PostMapping("/addDocument/{customerId}")
	public ResponseEntity<?> addDocument(@PathVariable long customerId, @RequestBody Documents document) {
		Documents newDocument = documentService.addDocumentByuserId(customerId, document);
		return new ResponseEntity<Documents>(newDocument, HttpStatus.CREATED);
	}

	@GetMapping("/getInsurenceActivePlanList")
	public ResponseEntity<?> getActiveInsurancePlans() {
		return new ResponseEntity<List<InsurancePlan>>(insurancePlanService.getActiveInsurancePlan(), HttpStatus.OK);

	}

	@GetMapping("/getInsurencePlan/{insurancePlanId}")
	public ResponseEntity<?> getInsurancePlanById(@PathVariable long insurancePlanId) {
		try {
			return new ResponseEntity<InsurancePlan>(insurancePlanService.getInsurancePlanById(insurancePlanId),
					HttpStatus.OK);
		} catch (PlanNotPresentException e) {
			return new ResponseEntity<Exception>(e, HttpStatus.BAD_REQUEST);
		}

	}

	@PostMapping("/addQuery")
	public ResponseEntity<?> addQuery(@RequestBody Feedback feedback) {
		feedback.setFeedbackId(0);
		return new ResponseEntity<Feedback>(feedbackService.addFeedback(feedback), HttpStatus.CREATED);
	}

	@GetMapping("/getFeedbacks")
	public ResponseEntity<?> getFeedbacks() {
		return new ResponseEntity<List<Feedback>>(feedbackService.getFeedbacks(), HttpStatus.OK);
	}

	@GetMapping(value = "getImage/{image}")
	public void downloadImage(@PathVariable("image") String image, HttpServletResponse response) {
		try {
			InputStream imagedb = insuranceTypeService.getImage(path, image);
			response.setContentType(MediaType.IMAGE_JPEG_VALUE);
			try {
				StreamUtils.copy(imagedb, response.getOutputStream());
			} catch (IOException e) {

			}
		} catch (FileNotFoundException e) {

		}
	}

	@PostMapping("/addInsurancenAccount/{customerId}")
	public ResponseEntity<?> addInsurancenAccount(@RequestBody InsuranceAccount insurancenAccount,
			@PathVariable String customerId) {

		try {
			return new ResponseEntity<InsuranceAccount>(
					insuranceAccountService.addInsuranceAccount(insurancenAccount, Long.parseLong(customerId)),
					HttpStatus.CREATED);
		} catch (Exception e) {
			return new ResponseEntity<Exception>(e, HttpStatus.BAD_REQUEST);
		}

	}

	// creating order for payment
	@PostMapping("/createOrder")
	public ResponseEntity<?> createOrder(@RequestBody Map<String, Object> data) throws Exception {

		System.out.println("payment started..");
		double amt = Double.parseDouble(data.get("amount").toString());
		RazorpayClient client = new RazorpayClient("rzp_test_uz0TSGP9WGLAXg", "7MEKpAwkzuVJd0YSwA3JeFTk");

		JSONObject ob = new JSONObject();
		ob.put("amount", amt * 100);
		ob.put("currency", "INR");
		ob.put("receipt", "txn_123456");

		Order order = client.orders.create(ob);
		OrderResponse orderResponse = new OrderResponse();
		orderResponse.setId(order.get("id"));
		orderResponse.setAmount(order.get("amount"));
		orderResponse.setReceipt(order.get("receipt"));
		orderResponse.setBuyDate(order.get("created_at"));
		System.out.println("Order:" + order);
		return new ResponseEntity<OrderResponse>(orderResponse, HttpStatus.OK);
	}

	// Add Commission
	@PostMapping("/addCommission/{agentCode}")
	public ResponseEntity<?> addCommission(@RequestBody Commission commission, @PathVariable("agentCode") String agentCode) {

		try {
			return new ResponseEntity<Commission>(commissionService.addCommission(commission, agentCode),
					HttpStatus.CREATED);
		} catch (Exception e) {
			return new ResponseEntity<Exception>(e, HttpStatus.BAD_REQUEST);
		}

	}

	@PutMapping("/changePassword/{userId}")
	public ResponseEntity<?> changePassword(@RequestBody PasswordRequest passwordRequest, @PathVariable String userId)
			throws Exception {
		return new ResponseEntity<Customer>(customerService.changePassword(passwordRequest, Long.parseLong(userId)),
				HttpStatus.CREATED);
	}

	@GetMapping("/getInsuranceAccountsByCustomerId/{customerId}")
	public ResponseEntity<?> getInsuranceAccountsByCustomerId(@PathVariable String customerId) {

		return new ResponseEntity<List<InsuranceAccount>>(
				insuranceAccountService.getInsuranceAccountByCustomerId(Long.parseLong(customerId)), HttpStatus.OK);

	}

	@GetMapping("/getPaymentsByCustomerId/{customerId}")
	public ResponseEntity<?> getPaymentsByCustomerId(@PathVariable String customerId) {

		return new ResponseEntity<List<Payment>>(paymentService.getPaymentsByCustomerId(Long.parseLong(customerId)),
				HttpStatus.OK);

	}

	@PostMapping("/addPayment/{insuranceAccountId}")
	public ResponseEntity<?> addPayment(@RequestBody Payment payment, @PathVariable int insuranceAccountId) {

		try {
			return new ResponseEntity<Payment>(paymentService.addPayment(payment, insuranceAccountId),
					HttpStatus.CREATED);
		} catch (Exception e) {
			return new ResponseEntity<Exception>(e, HttpStatus.BAD_REQUEST);
		}

	}
	
	@PostMapping("/installmentCommission/{agentCode}/{insuranceSchemeId}")
	public ResponseEntity<?> installmentCommission(@RequestBody Commission commission, @PathVariable("agentCode") String agentCode,@PathVariable("insuranceSchemeId") long insuranceSchemeId) {

		try {
			return new ResponseEntity<Commission>(commissionService.installmentCommission(commission, agentCode,insuranceSchemeId),
					HttpStatus.CREATED);
		} catch (Exception e) {
			return new ResponseEntity<Exception>(e, HttpStatus.BAD_REQUEST);
		}

	}
	
	@PutMapping("/update")
	public ResponseEntity<?> addCustomer(@RequestBody Customer customer) {
		return new ResponseEntity<Customer>(customerService.addCustomer(customer), HttpStatus.CREATED);
		
	}
	
	@GetMapping("/getCustomerProfile/{customerId}")
	public ResponseEntity<?> getCustomerProfile(@PathVariable String customerId) {

		try {
			return new ResponseEntity<Customer>(
					customerService.getCustomerById(Long.parseLong(customerId)), HttpStatus.OK);
		} catch (NumberFormatException | UserNotValidException e) {
			return new ResponseEntity<Exception>(e, HttpStatus.BAD_REQUEST);
		}

	}

}
