package com.einsurance.insurence.responseBody;

import java.util.Date;

import lombok.Data;

@Data
public class OrderResponse {
	private int amount;
	private String id;
	private String receipt;
	private Date buyDate;
}
