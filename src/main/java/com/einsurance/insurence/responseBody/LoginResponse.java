package com.einsurance.insurence.responseBody;

import lombok.Data;

@Data
public class LoginResponse {
	long id;
	String name;
	String role;
}
